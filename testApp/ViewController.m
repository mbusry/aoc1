//
//  ViewController.m
//  testApp
//
//  Created by Michael on 8/27/13.
//  Copyright (c) 2013 Michael Usry. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
 // set the background to white
self.view.backgroundColor = [UIColor whiteColor];
    
    

// startup screen
    un = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 32)];
    
    if (un != nil){
        un.text = @"Username: ";
        un.textAlignment = NSTextAlignmentLeft;
        un.textColor = [UIColor blackColor];
    }
    
// user input for login name
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(110, 10, 190, 32)];

    if (textField !=nil){
        textField.borderStyle = UITextBorderStyleRoundedRect;
        [self.view addSubview:textField];

    }
    
// Login button: when clicked it goes to onClick
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    if (button !=nil) {
        button.frame = CGRectMake(235, 50, 65, 44);
        [button setTitle:@"Login" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(onClick) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:button];
    }
    
// text area for the instructions and reply after the input
    
    commandLevel = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 320, 64)];
    
    if (commandLevel != nil){
        commandLevel.backgroundColor = [UIColor lightGrayColor];
        commandLevel.textColor = [UIColor blueColor];
        commandLevel.text = @"Please Enter Username";
        commandLevel.textAlignment = NSTextAlignmentCenter;
        commandLevel.numberOfLines = 2;
        
    }
    
//button for today's date
    
    UIButton* dateButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    if (dateButton !=nil) {
        dateButton.frame = CGRectMake(10, 200, 90, 44);
        [dateButton setTitle:@"Show Date" forState:UIControlStateNormal];
        [dateButton addTarget:self action:@selector(showDate) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:dateButton];
    }
    
// info button to show the author's name
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    
    if (infoButton != nil) {
        infoButton.frame = CGRectMake(10,300,30,30);
        infoButton.tintColor = [UIColor blackColor];
        [infoButton addTarget:self action:@selector(onInfo) forControlEvents:UIControlEventTouchUpInside];

        
        [self.view addSubview:infoButton];
    }
    
// uilabel that show the author's name
    
    infoBox = [[UILabel alloc] initWithFrame:CGRectMake(0, 350, 320, 64)];
    
    if (infoBox != nil){
        infoBox.textAlignment = NSTextAlignmentLeft;
        infoBox.numberOfLines = 2;

        [self.view addSubview:infoBox];
  
    }



    
    
    [self.view addSubview:un];
    [self.view addSubview:commandLevel];
    [self.view addSubview:infoBox];

    

}

// When inputting the username this replaces the original text with the inputted text and check if there if no input

-(void)onClick
{
    if (textField.text !=nil) {
    NSString* messageText = [NSString stringWithFormat: @"User: %@ has been logged in.",textField.text];
        commandLevel.backgroundColor = [UIColor lightGrayColor];
        commandLevel.textColor = [UIColor blueColor];
        commandLevel.textAlignment = NSTextAlignmentCenter;
        commandLevel.numberOfLines = 2;
        commandLevel.text = messageText;
    }else{
        commandLevel.backgroundColor = [UIColor redColor];
        commandLevel.textColor = [UIColor blackColor];
        commandLevel.text = @"Username cannot be empty";
        commandLevel.textAlignment = NSTextAlignmentCenter;
        
        
    }
}

// Apps author name

-(void)onInfo
{
    infoBox.text = @"This application was created by: Michael Usry";
    
}

// show's today's date in an alertview.
-(void)showDate
{
    NSDate *date = [NSDate date];
    
    NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
    if (dateFormatter != nil)
    {
        [dateFormatter setDateStyle:(NSDateFormatterFullStyle)];
        NSString* todayDate = [dateFormatter stringFromDate:date];
        UIAlertView *alertMe = [[UIAlertView alloc] initWithTitle:@"Date" message:todayDate delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        if (alertMe != nil) {
            [alertMe show];
        }


    }
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
